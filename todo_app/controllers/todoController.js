var bodyParser = require("body-parser");
var mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017");

// Schema
var todoSchema = new mongoose.Schema({
  item: String,
  dateAdded: {
    type: Date,
    default: Date.now()
  },
});

// Model
var Todo = mongoose.model("Todo", todoSchema);

var urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function (app) {
  app.get("/todo", function (req, res) {
    /**
     * Read todos from db and populate the frontend with data.
     */
    Todo.find({}, function (err, data) {
      if (err) throw err;
      res.render("todo", { todos: data });
    });
  });

  app.post("/todo", urlencodedParser, function (req, res) {
    /**
     * Get data from the frontend and save to db.
     */
    var newTodo = Todo(req.body).save(function (err, data) {
      if (err) throw err;
      res.json(data);
    });
  });

  app.delete("/todo/:item", function (req, res) {
    /**
     * Find and delete the data from db.
     */
    console.log(req.params.item.replace(/\-/g, " ").trim());
    Todo.find({ item: req.params.item.replace(/\-/g, " ").trim() }).deleteOne(
      function (err, data) {
        if (err) throw err;
        console.log(data);
        res.json(data);
      }
    ); 
  });
};
