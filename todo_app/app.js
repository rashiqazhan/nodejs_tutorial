var express = require("express");
var todoController = require("./controllers/todoController");

var app = express();

// template engine
app.set("view engine", "ejs");

// static files
app.use(express.static("./public"));

// controllers
todoController(app);

// define ports
app.listen(3005);
console.log("listening to port 3005");
