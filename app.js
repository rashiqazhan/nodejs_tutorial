var express = require("express");
var app = express();
app.set("view engine", "ejs")
app.use('/assets', express.static(assets));

app.get("/", function (req, res) {
    res.send("Hello World");
});

app.get("/profile/:name", function (req, res) {
    data = {
        name: req.params.name,
        hobbies: ['a', 'b', 'c'],
    }
    res.render("profile", data);
});

app.get("/contact", function(req, res){
    res.render('profile', {qs: res.query})
})   

app.listen(3004);